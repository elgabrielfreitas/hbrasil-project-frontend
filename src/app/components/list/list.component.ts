import { Component } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';

@Component({
    selector: 'app-root',
    templateUrl: './list.component.html',
    styleUrls: ['../../../assets/demo/badges.scss']
})
export class ListComponent {

    calendarOptions: CalendarOptions = {
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
          },
        initialView: 'dayGridMonth',
      };

}
